﻿using System.Collections.Generic;

namespace SpellCheck
{
    public class FlaggedToken
    {
        public int Offset { get; set; }
        public string Token { get; set; }
        public string Type { get; set; }
        public IEnumerable<SuggestionResult> Suggestions { get; set; }
    }
}