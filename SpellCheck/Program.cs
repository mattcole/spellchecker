﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SpellCheck
{
    internal class Program
    {
        private static HttpClient _httpClient = null;
        public static void Main(string[] args)
        {
            try
            {
                MainAsync().Wait();
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static async Task MainAsync()
        {
            var claimsDesc = "MY JEW WAS STOLEN BY MY HELPER , AS THIS HAPPENED IN A SPACE OF TWO MTHS , I ASKED HER ON THE 28TH OF FEB , IF SHE STOLE THE JEW OR NOT AND MY HELPER SAID NO , I THAN ASKED HER THAT I WILL GET SOME ONE TO DO A POLGRAPHIC TE ST AND SHE SAID NO , SHE IS NOT WILL TO GET IT , IT COULD ONLY BE HER AS I LIVEWITH MY HUBBY AND MY SELF AND THE MAID, AS SHE USED TO COME IN EVERYS DAY AND AT THE NITE , MY JEW BOX WAS ON THE DRESSING TABLE IN THE BEDROOM , AS SHE HAD EXCESS TO THE BEDROOM TOO, I HAVE KNOWN HER FOR 17YRS , BUT I AM NOT SURE WHAT MADE HER DO THIS , AS SHE NEVER SAD YES SHE STOLE THE IT EMS , HER LINAH 0723061772 , AND I HAVE NOT BEEN TO THE POLICE BUT WE WAN T TO THE CCMA , AS I AKED HER TO STOP WORKING , I HAVE BEEN STAYING HERE F ROM 2002 , PREV CLAIM : NONE. PREV INS : 10YRS WITH OUTSURANCE , -------------------------------------------------------------------------";
            Console.WriteLine(claimsDesc);
            var spellCheckerResult = await _GetCorrectedSpelling(claimsDesc);
            Console.WriteLine(spellCheckerResult);
        }

        private static async Task<string> _GetCorrectedSpelling(string textToCorrect)
        {
            var httpClient = _GetHttpClientForCorrections();

            var response = await httpClient.GetAsync($"spellcheck?text={WebUtility.UrlEncode(textToCorrect)}&precontext=insurance");
            response.EnsureSuccessStatusCode();
            return _UpdateWithCorrections(textToCorrect, response);
        }

        private static string _UpdateWithCorrections(string textToCorrect, HttpResponseMessage response)
        {
            var value = response.Content.ReadAsStringAsync().Result;
            Console.WriteLine(value);

            var result = JsonConvert.DeserializeObject<SpellCheckResponse>(value);

            if (!result.FlaggedTokens.Any()) return textToCorrect;

            return new SpellCorrector().CorrectErrors(textToCorrect, result.FlaggedTokens);
        }

        private static HttpClient _GetHttpClientForCorrections()
        {
            var baseUrl = "https://api.cognitive.microsoft.com/bing/v5.0/";
            if(_httpClient != null) return _httpClient;
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri(baseUrl);
            _httpClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "851764f8c65f4228a385fc878d23fc82");

            return _httpClient;
        }
    }
}