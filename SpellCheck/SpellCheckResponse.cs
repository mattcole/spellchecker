﻿using System.Collections.Generic;

namespace SpellCheck
{
    public class SpellCheckResponse
    {
        public IEnumerable<FlaggedToken> FlaggedTokens { get; set; }
    }
}