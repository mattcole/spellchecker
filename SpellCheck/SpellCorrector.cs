﻿using System.Collections.Generic;
using System.Linq;

namespace SpellCheck
{
    public class SpellCorrector
    {
        public string CorrectErrors(string original, IEnumerable<FlaggedToken> corrections)
        {
            var correctedText = original;
            foreach (var token in corrections.OrderByDescending(c => c.Offset))
            {
                if (token.Suggestions.Any(s => s.Score > 0.7))
                {
                    var topSuggestion = token.Suggestions.OrderByDescending(s => s.Score).First();
                    correctedText = _ReplaceToken(correctedText, token.Offset, token.Token, topSuggestion.Suggestion);
                }
            }

            return correctedText;
        }

        private string _ReplaceToken(string originalText, int tokenOffset, string originalToken, string newText)
        {
            var lengthOfOriginalToken = originalToken.Length;
            var beforeError = originalText.Substring(0, tokenOffset);
            var afterError = originalText.Substring(tokenOffset + lengthOfOriginalToken);

            return $"{beforeError}{newText}{afterError}";
        }
    }
}