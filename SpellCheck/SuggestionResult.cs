﻿namespace SpellCheck
{
    public class SuggestionResult
    {
        public string Suggestion { get; set; }
        public double Score { get; set; }
    }
}