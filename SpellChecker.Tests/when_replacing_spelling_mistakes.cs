﻿using System.Diagnostics;
using NUnit.Framework;
using Shouldly;
using SpellCheck;


namespace SpellChecker.Tests
{
    [TestFixture]
    public class when_replacing_spelling_mistakes
    {
        private string _correctedText;

        [TestFixtureSetUp]
        public void Context()
        {
            var misspelling = "thsis si a mistkae";
            var tokens = new[]
            {
                new FlaggedToken
                {
                    Offset = 0,
                    Token = "thsis",
                    Suggestions = new[] {new SuggestionResult {Score = 1, Suggestion = "this"}},

                },
                new FlaggedToken
                {
                    Offset = 6,
                    Token = "si",
                    Suggestions = new[] {new SuggestionResult {Score = 1, Suggestion = "is"}}
                },
                new FlaggedToken
                {
                    Offset = 11,
                    Token = "mistkae",
                    Suggestions = new[] {new SuggestionResult {Score = 1, Suggestion = "mistake"}}
                },
            };

            var corrector = new SpellCorrector();
            _correctedText = corrector.CorrectErrors(misspelling, tokens);

        }

        [Test]
        public void all_mistakes_are_corrected()
        {
            _correctedText.ShouldBe("this is a mistake");
        }
    }
}